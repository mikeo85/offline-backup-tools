```
 ██████╗ ███████╗███████╗██╗     ██╗███╗   ██╗███████╗                                   
██╔═══██╗██╔════╝██╔════╝██║     ██║████╗  ██║██╔════╝                                   
██║   ██║█████╗  █████╗  ██║     ██║██╔██╗ ██║█████╗                                     
██║   ██║██╔══╝  ██╔══╝  ██║     ██║██║╚██╗██║██╔══╝                                     
╚██████╔╝██║     ██║     ███████╗██║██║ ╚████║███████╗                                   
 ╚═════╝ ╚═╝     ╚═╝     ╚══════╝╚═╝╚═╝  ╚═══╝╚══════╝                                   
                                                                                         
██████╗  █████╗  ██████╗██╗  ██╗██╗   ██╗██████╗     ██████╗ ██████╗ ██╗██╗   ██╗███████╗
██╔══██╗██╔══██╗██╔════╝██║ ██╔╝██║   ██║██╔══██╗    ██╔══██╗██╔══██╗██║██║   ██║██╔════╝
██████╔╝███████║██║     █████╔╝ ██║   ██║██████╔╝    ██║  ██║██████╔╝██║██║   ██║█████╗  
██╔══██╗██╔══██║██║     ██╔═██╗ ██║   ██║██╔═══╝     ██║  ██║██╔══██╗██║╚██╗ ██╔╝██╔══╝  
██████╔╝██║  ██║╚██████╗██║  ██╗╚██████╔╝██║         ██████╔╝██║  ██║██║ ╚████╔╝ ███████╗
╚═════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝ ╚═════╝ ╚═╝         ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝  ╚══════╝
```
<!-- Ascii Text: https://www.patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=Offline%0ABackup%20Drive -->

# OFFLINE BACKUP DRIVE
> An offline, airgapped backup of critical data

This drive contains offline copies of important backup datasets. See below for specifics.

**!!! DRIVE CONTAINS UNENCRYPTED SENSITIVE DATA --- STORE IN A SECURE PHYSICAL LOCATION !!!**

**Contents**
- [OFFLINE BACKUP DRIVE](#offline-backup-drive)
  - [Top-Level Organization](#top-level-organization)
  - [Backup Data Directories](#backup-data-directories)
    - [(Example)](#example)

## Top-Level Organization
```
.
├── airgap/         # Script + supports for creating easy, repeatable backups to this drive
├── backup-data/    # Storage for all backup datasets, organized in various subdirectories
└── README.md       # Description of backup drive and contents (this file)
```

## Backup Data Directories

### (Example)

**Description:**
- (Add Description)

**Backup Process:**
- (Add Process)

**Recovery Process:**
- *Credentials Needed:* (None or where they reside)
- (Add Process)