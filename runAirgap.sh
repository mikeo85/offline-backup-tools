#!/bin/bash

#########################################################
#  RUN AIRGAP
#  ----------
#
#        Written by: Mike Owens
#        Email:      mikeowens (at) fastmail (dot) com
#        Website:    https://michaelowens.me
#        GitLab:     https://gitlab.com/qu13t0ne
#        GitHub:     https://github.com/qu13t0ne
#        Mastodon:   https://infosec.exchange/@qu13t0ne
#        X (Twitter):https://x.com/qu13t0ne
#
#  First Run: Sets up the backup destination directory
#        and configuration files.
#
#  Subsequent Runs: Uses rsync to copy data from targets 
#        (as listed in the configuration file) to the 
#        destination directory.
#
#  Ref: https://gitlab.com/qu13t0ne/airgap
#  
#########################################################

# ===== FUNCTIONS ==============================
function get_abs_filename() {
  # $1 : relative filename
  echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}

function logAndConsole () {
    echo $(date +%Y/%m/%d\ %T) "$1" >> $logFile
    echo "$1"
}

# ===== VARIABLES ==============================
scriptDir="$(dirname "$0")"
backupDestin="$scriptDir/../backup-data"
confFile="$scriptDir/../airgapFileList.conf"
rootReadme="$scriptDir/../README.md"
logPath="$scriptDir/../logfiles"
logFile="$logPath/airgap-$(date +%Y%m%dT%H%M%S).log"
headerArt="$scriptDir/.templates/headerArt"
rsyncOptions="-rltDhz --protect-args --log-file=$logFile --stats --progress"
declare -a backupSources=()

# ===== MAIN ==============================

cat $headerArt

if [[ -d "$backupDestin" ]] && [[ -f "$confFile" ]] && [[ -d "$logPath" ]]; then
    while IFS= read -r line; do
        if [[ "$line" != "" ]] && [[ "$line" != "#"* ]]; then
            backupSources+=("$line")
        fi
    done < $confFile
    if [[ ${#backupSources[@]} -gt 0 ]]; then
        cat $headerArt >> $logFile
        logAndConsole "BEGINNING AIRGAP BACKUP"
        logAndConsole "Syncing the following directories and files to $(get_abs_filename $backupDestin):"
        for each in "${backupSources[@]}"; do
            logAndConsole "- $each"
        done
        rsync $rsyncOptions "${backupSources[@]}" "$backupDestin"
        logAndConsole "AIRGAP BACKUP COMPLETE"
    else
        echo "Error: No source paths are defined in $(get_abs_filename $confFile)"
        echo "Terminating..."
        exit 1
    fi
else
    echo "Performing initial setup."
    if ! [[ -d "$backupDestin" ]]; then
        mkdir "$backupDestin"
        echo "... Created backup destination directory:             $(get_abs_filename $backupDestin)"
    else
        echo "... Backup destination directory already exists:      $(get_abs_filename $backupDestin)"
    fi
    if ! [[ -d "$logPath" ]]; then
        mkdir "$logPath"
        echo "... Created logs directory:                           $(get_abs_filename $logPath)"
    else
        echo "... Logs directory already exists:                    $(get_abs_filename $logPath)"
    fi
    if ! [[ -f "$confFile" ]]; then
        cat $scriptDir/.templates/filelist.sample > "$confFile"
        echo "... Created configuration file:                       $(get_abs_filename $confFile)"
    else
        echo "... Configuration file already exists:                $(get_abs_filename $confFile)"
    fi
    if ! [[ -f "$rootReadme" ]]; then
        cat $scriptDir/.templates/README.sample.md > "$rootReadme"
        echo "... Created README file for the backup drive:         $(get_abs_filename $rootReadme)"
        echo "    Use this file to document the drive contents and the recovery process for everything getting backed up here."
    else
        echo "... README file for the backup drive already exists:  $(get_abs_filename $rootReadme)"
    fi
    echo "
Initial setup done.

ACTION REQUIRED:
  - Edit the configuration file to list all files, directories, or wildcards
    that should be included and/or excluded from the backup process.
  - Once the configuration file is ready, re-run this script to perform the backup.
"
fi

# eof